local vim = vim
-- lsp setup
-- Set Default Prefix.
-- Note: You can set a prefix per lsp server in the lv-globals.lua file
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = {
      prefix = "",
      spacing = 0,
    },
    signs = true,
    underline = true,
  }
)



-- -- uncomment below to enable nerd-font based icons for diagnostics in the
-- -- gutter, see:
-- -- https://github.com/neovim/nvim-lspconfig/wiki/UI-customization#change-diagnostic-symbols-in-the-sign-column-gutter
 local signs = { Error = " ", Warning = " ", Hint = " ", Information = " " }

 for type, icon in pairs(signs) do
   local hl = "LspDiagnosticsSign" .. type
   vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
 end

-- symbols for autocomplete
vim.lsp.protocol.CompletionItemKind = {
    "   (Text) ",
    "   (Method)",
    "   (Function)",
    "   (Constructor)",
    " ﴲ  (Field)",
    "[] (Variable)",
    "   (Class)",
    " ﰮ  (Interface)",
    "   (Module)",
    " 襁 (Property)",
    "   (Unit)",
    "   (Value)",
    " 練 (Enum)",
    "   (Keyword)",
    "   (Snippet)",
    "   (Color)",
    "   (File)",
    "   (Reference)",
    "   (Folder)",
    "   (EnumMember)",
    " ﲀ  (Constant)",
    " ﳤ  (Struct)",
    "   (Event)",
    "   (Operator)",
    "   (TypeParameter)"
}

--local function setup_servers()
--  local servers = {'clangd', 'vimls'}
--  for _, server in pairs(servers) do
--    require'lspconfig'[server].setup{}
--  end
--end
--
--setup_servers()
local lsp_installer = require("nvim-lsp-installer")

-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
lsp_installer.on_server_ready(function(server)
    local opts = {}
    if server.name== "sumneko_lua" then
        server.cmd = {'/usr/bin/lua-language-server', "-E", '/' .. "/main.lua"}
        server.settings = {
            Lua = {
                runtime = { version = 'LuaJIT', path = '/usr/bin/luajit', },
                diagnostics = { globals = {'vim'}},
                workspace = { library = vim.api.nvim_get_runtime_file("", true) },
                telemetry = { enable = false },

            }
        }
    end
    -- (optional) Customize the options passed to the server
    -- if server.name == "tsserver" then
    --     opts.root_dir = function() ... end
    -- end
    -- if server.name == "rust_analyzer" then
    --     -- Initialize the LSP via rust-tools instead
    --     require("rust-tools").setup {
    --         server = vim.tbl_deep_extend("force", server:get_default_options(), opts),
    --     }
    --     server:attach_buffers()
    -- else
    -- -- This setup() function is exactly the same as lspconfig's setup function.
    -- -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
        server:setup(opts)
    -- end
end)


require('formatter').setup({
    filetype = {
        rust = {
              -- Rustfmt
              function()
                return {
                  exe = "rustfmt",
                  args = {"--emit=stdout"},
                  stdin = true
                }
              end
        },
        cpp = {
            --clang-format
            function()
                return {
                    exe = "clang-format",
                    args = {"--assume-filename", vim.api.nvim_buf_get_name(0)},
                    stdin = true,
                    cwd = vim.fn.expand('%:p:h')  -- Run clang-format in cwd of the file.
                }
            end
        },
    }
})
