call plug#begin('~/.config/nvim/autoload')

    " better sytantax highlighting
    Plug 'tikhomirov/vim-glsl'
    " Status line
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    " nvim-lspConfig conversion
    Plug 'neovim/nvim-lspconfig'
    Plug 'nvim-lua/completion-nvim'
    Plug '9mm/vim-closer'
    Plug 'tpope/vim-commentary'
    Plug 'lifepillar/gruvbox8'
    Plug 'tweekmonster/startuptime.vim'
    Plug 'mizlan/termbufm'
    Plug 'williamboman/nvim-lsp-installer'

call plug#end()
