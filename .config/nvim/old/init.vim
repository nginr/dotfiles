"Vim plug plugins
source $HOME/.config/nvim/vim-plug/plugins.vim
" Setting && Key mappings
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim
" Color schemes
source $HOME/.config/nvim/themes/airline.vim
" Plugin Configs
"source $HOME/.config/nvim/plug-config/coc.vim
source $HOME/.config/nvim/plug-config/cmake.vim

" lua
" Diagnostics
source $HOME/.config/nvim/lsp/diagnostic-completion.vim
