" Better nav
"inoremap <expr> <c-j> ("\<C-n>")
"inoremap <expr> <c-k> ("\<C-p>")

" Use alt + hjkl to resize
nnoremap <M-j> :resize +2<CR>
nnoremap <M-k> :resize -2<CR>
nnoremap <M-h> :vertical resize -2<CR>
nnoremap <M-l> :vertical resize +2<CR>

" Close the current buffer
nnoremap <M-x> :bd<CR>
nnoremap <M-b> :buffers<CR>


" ESCAPE jk kj
inoremap jk <Esc>
inoremap kj <Esc>

inoremap <c-u> <Esc> viw~i
nnoremap <c-u> viw~<Esc>

" Tab in general mode will move to next buffer
nnoremap  <TAB>  :bnext<CR>
" SHIFT TAB will go back
nnoremap <S-TAB> :bprevious<CR>

"Alternate way to save
nnoremap <M-s> :w  <CR>
" Alternate way to quit
nnoremap <M-q> :wq!<CR>
" TAB Completion
""inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>""""""

" Better Tabbing
vnoremap < <gv
vnoremap > >gv

" ; :
" : ;
nnoremap ; :
nnoremap : ;




" BETTER nav
"nnoremap <C-h> <C-w>h
"nnoremap <C-j> <C-w>j
"nnoremap <C-k> <C-w>k
"nnoremap <C-l> <C-w>l
