let g:cmake_link_compile_commands = 1


nnoremap <leader>cg :CMakeGenerate<cr>
nnoremap <leader>cb :CMakeBuild<cr>
nnoremap <leader>cc :CMakeClean<cr>
