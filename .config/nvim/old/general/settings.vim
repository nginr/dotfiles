" set leader key
let mapleader=" "
nn <silent> <leader>n :noh<CR>

set hid nowrap ru sb spr sts=4 sw=4 et nu cul rnu ic
set nu cul rnu ic icm=split scs list nosmd swb=useopen scl=yes icm=split tgc
set si ai stal=4 nobk nowb ut=300 sta
set pumheight=10 lcs=tab:┆·,trail:·,precedes:,extends:
set fileencoding=utf-8
set mouse=a
set timeoutlen=100                  " Timeoutlen by default is 1000 "

colo gruvbox8
set bg=dark
set cot=menuone,noinsert,noselect shm+=c

autocmd! BufNewFile,BufRead *.vs, *.fs, *.frag, set ft=glsl



" auto source when writing to init.vim
au! BufWritePost $VIMRC source %

" deleting trailing white space after save
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\$/\r/e

" Don't offer to open certain files/directories
set wildignore+=*.bmp,*.gif,*.ico,*.jpg,*.png,*.ico
set wildignore+=*.pdf,*.psd
set wildignore+=node_modules/*,bower_components/*

let g:colorizer_auto_filetype='css,html,conf'
