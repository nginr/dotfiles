# The following lines were added by compinstall


zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate

autoload -Uz compinit && compinit

autoload -U colors && colors


#PROMPT="%F{047}%B[%d]%f"
PROMPT="%F{blue}%n in [%2~]%f %F{yellow}%f "

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats "%F{red} %b%f %S %a %m%u%c"


fpath+=./.zfunc/


# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt autocd extendedglob notify completeinword
bindkey -v
setopt histignorealldups

export ZSHRC=~/.config/zsh/.zshrc

mkcd(){mkdir -p $1 && cd $1;}


alias gtst='git status'
alias ls='ls -N --group-directories-first --color=always -X'
alias ll='ls -lAhN --color=always --group-directories-first -X'
alias lm='clear'
alias nn='nvim'
alias mkdir='mkdir -p'
alias csway='nvim ~/.config/sway/config'
alias so='source ~/.config/zsh/.zshrc'
alias cenv='sudo nvim /etc/environment'
alias ..='cd ..'
alias pacs='pacman -Ss'
alias p='sudo pacman'
alias hdd-mt='sudo mount /dev/sda1 ~/hdd/'
alias hdd-umt='sudo umount ~/hdd/'
alias a='./a.out'
alias grep='GREP_COLOR="1;33;40" LANG=C grep --exclude-dir=node_modules --color=auto'
