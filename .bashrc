
# ~/.bashrc
#
# If not running interactively, don't do anything

#PS1='[\u@\h \W] -> '

[[ $- != *i* ]] && return

alias hist='cat ~/.fish_history | grep'
alias pitop='bpytop'
alias clang++='clang++ -stdlib=libc++ -fuse-ld=lld'
yellow='\e[0;33m'
red="\e[0;91m"
blue="\e[0;94m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
green="\e[1;92m"
white="\e[0;97m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"
white_bold="\e[1;37m"
darkgrey_bg="\e[1;40m"
cyan="\e[0;36m"

alias nn="nvim"
alias cbrun="rm ./* && cmake ../ &&"
#PS1="<$green\u$reset$darkgrey_bg$white_bold@$reset$expand_bg$yellow\h:$reset> $cyan\w$reset\nλ  "
PS1="<$green\u$reset$darkgrey_bg$white_bold@$reset$expand_bg$yellow\h:$reset> $cyan\w$reset\n  𝝺   "


alias cenv='sudo nvim /etc/environment'
alias cbar='nvim ~/.config/waybar/config'
alias csway='nvim ~/.config/sway/config'
alias a='./a.out'
alias ls='ls -hN --color=auto --group-directories-first'
alias grep='grep --color=auto'
alias mkdir='mkdir -pv'
alias sxiv='sxiv -b'
alias ll='ls --color=auto -lA'
alias c='cd ~'
alias ..='cd ..'
alias lm='clear'
alias wifi='nmcli device wifi list --rescan yes'
alias cmakelist='nvim CMakeLists.txt'

alias so='source ~/.bashrc'
alias cvim='nvim $VIMRC'
alias sv='source $VIMRC'

alias pl='cd -'
alias zz='exit'


alias pacs='pacman -Ss'
alias p='sudo pacman'

alias tpg='sudo systemctl start postgresql.service'
alias spg='systemctl status postgresql.service'
alias fpg='sudo systemctl stop postgresql.service'

alias tcup='sudo systemctl start cups'
alias scup='systemctl status cups'
alias fcup='sudo systemctl stop cups'

alias so7c35='nmcli device wifi connect 'SO7C35' password 'Hy3hc3g#''



# variable for cargo
alias ci='cargo init'
alias cn='cargo new'
alias cb='cargo build'
alias cc='cargo check'
alias cr='cargo run'

acpi
